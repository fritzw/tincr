var ProjectUtils = {
    scanDirectory : function(root, path, onFile, onEnd){

        var walkTree = function(dir, callback){
            Gito.FileUtils.ls(dir, function(entries){
                entries.asyncEach(function(entry, done){
                    if (entry.isFile){
                        onFile(entry);
                        done();
                    }
                    else{
                        walkTree(entry, done);
                    }
                }, callback);
            });
        }
        if (path){
            root.getDirectory(path, {create:false}, function(dir){
                walkTree(dir, onEnd);
            }, onEnd);
        }
        else
        {
            walkTree(root, onEnd);
        }
    },
    getFileUrlPaths : function(rawUrl, urlCache){
        // strip the query string assuming that not many people put 
        // question marks in their file names
        var url = rawUrl.split("?")[0];
        url = decodeURI(url);
        if (url.indexOf('file://') == 0){
            var path;
            if (url.indexOf('localhost') == 7){
                path = url.substring(16);
            }
            else{
                path = url.substring(7);
            }
            if (navigator.platform.indexOf('Win') == 0 && path.charAt(0) == '/'){
                path = path.substring(1);
            }

            var obj = {autoSave : path};
            urlCache[url] = obj;
            return obj;
        }
        return {};
    },
    stripQueryString : function(rawUrl){
        var url = rawUrl.split("?")[0];
        return url;
    },
    clearUrlCache : function(rawUrl, stripQuery, urlCache, fileCache){
        var url = stripQuery ? ProjectUtils.stripQueryString(rawUrl) : rawUrl;
        var data = urlCache[url];
        if (data){
            if (data.autoReload){
                for (var i = 0; i < data.autoReload.length; i++){
                    delete fileCache[data.autoReload[i]];
                }
            }
            else if (data.autoSave){
                delete fileCache[data.autoSave];
            }
        }
    },
    cacheRelationship : function(rawUrl, obj, stripQuery, urlCache, fileCache){
        url = stripQuery ? ProjectUtils.stripQueryString(rawUrl) : rawUrl;
        urlCache[url] = obj;
        if (obj.autoSave){
            fileCache[obj.autoSave] = rawUrl;
        }
        else if (obj.autoReload){
            for (var i = 0; i < obj.autoReload.length; i++){
                fileCache[obj.autoReload[i]] = rawUrl;
            }
        }
    }  
};